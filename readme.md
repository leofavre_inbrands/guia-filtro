Funcionalidade Filtro
=====================

O filtro é composto por duas áreas: a de resumo e a de seleção. A área de resumo é posicionada acima da arara de produtos, e a área de seleção é posicionada à sua esquerda.

![Regiões principais do filtro](http://www.richards.com.br/filtro/rch-filtro-01.png?abc)

Área de resumo
--------------

A área de resumo é composta por um texto que indica o total de produtos encontrados com a seleção atual do filtro, seguido pela descrição dos seletores ativos.

O total de produtos fica alinhado à esquerda em relação à área de seleção do filtro e a descrição fica alinhada à esquerda em relação aos produtos da arara.

![Componentes da área de resumo](http://www.richards.com.br/filtro/rch-filtro-05.png?abc)

A descrição dos seletores ativos é um frase formada por cada critério filtrado, seguida de seus respectivos seletores ativos. Os seletores são separados por "ou", e os critérios por "," ou "e". A imagem a seguir ilustra o comportamento desejado.

![Variações da área de resumo](http://www.richards.com.br/filtro/rch-filtro-06.png?abdc)

O clique num seletor o remove e a página é imediatamente recarregada com o novo resultado.

Área de seleção
---------------

A área de seleção é composta por critérios, que são representados por caixas. Cada critério tem um título, uma área expansível e um botão "limpar".

![Componentes da área de seleção](http://www.richards.com.br/filtro/rch-filtro-02.png?abc)

Cada área expansível tem um ou mais seletores, que podem ser: checkbox com texto lateral, checkbox com texto interno, checkbox de seleção de cor e seletor de faixa de preço. Cada seletor do tipo checkbox tem cinco estados possíveis: indisponível, disponível, disponível hover, disponível selecionado e disponível selecionado hover.

![Checkboxes](http://www.richards.com.br/filtro/rch-filtro-03.png?abc)

O seletor de faixa de preço tem aparência e comportamento diferenciados, descritos mais a frente no documento.

![Seletor de faixa de preço](http://www.richards.com.br/filtro/rch-filtro-04.png?abcd)

### Comportamento na tela

Dependendo do tamanho do filtro, ele pode adotar um dos comportamentos a seguir, em relação à tela:

##### Sticky

Se for um filtro pequeno, a área de seleção e o total de produtos devem grudar no topo da tela enquanto ocorre a rolagem. Nesse caso, apenas uma área expansível pode ficar aberta de cada vez, então a expansão de uma delas retrai as demais automaticamente.

A primeira caixa de critério é expandida por padrão, portanto, é ideal que seja a mais relevante.

![Comportamento Sticky](http://www.richards.com.br/filtro/rch-filtro-07.png?abc)

##### Expandido

Se for um filtro grande demais para caber na altura da tela, todas as áreas expansíveis devem permanecer expandidas o tempo todo, e o filtro deve seguir a rolagem da página naturalmente. Nesse caso, as setas das áreas expansíveis são removidas do lay-out.

![Comportamento Sticky](http://www.richards.com.br/filtro/rch-filtro-08.png?abc)

### Comportamento das áreas expansíveis

As áreas expansíveis são expandidas ou retraídas no clique da seta em sua lateral esquerda, alternadamente. A direção da seta aponta para cima quando a área estiver expandida, e para baixo quando estiver retraída.

![Comportamento Sticky](http://www.richards.com.br/filtro/rch-filtro-09.png?abc)

### Comportamento dos seletores checkbox

##### Em relação ao critério

Os seletores dentro de um critério são includentes: é possível ativar mais de um seletor ao mesmo tempo.

![Seleção includente](http://www.richards.com.br/filtro/rch-filtro-10.png?abc)

A ativação de qualquer seletor dentro de um critério faz com que o botão "limpar" apareça ao lado do título. Esse botão é um atalho para desativar todos os seletores do critério ao mesmo tempo. Não havendo seletores ativos, o botão "limpar" desaparece.

![Seleção includente](http://www.richards.com.br/filtro/rch-filtro-11.png?abc)

Não existe um botão de "aplicar filtros", salvo no seletor de preço. A resposta da seleção não é imediata e depende de um temporizador, cuja lógica é explicada em detalhes a seguir.

É importante notar que desativar todos os seletores dentro de um critério tem exatamente o mesmo efeito que ativar todos ao mesmo tempo. **Não filtrar nada e filtrar por todos gera o mesmo resultado**.

##### Em relação a outros critérios

Os seletores ativos de um critério limitam o número possível de resultados de outros. Quando isso ocorre, os seletores disponíveis e não ativados que venham a gerar resultados nulos ao serem ativados, devem passar para o estado "indisponível" automaticamente, sem que seja necessário recarregar a página. **Atenção, os seletores indisponíveis não desaparecem em nenhum momento**.

No exemplo a seguir, quando o cliente seleciona um determinada cor, o sistema retorna que não há os tamanhos 46 e 48 para a cor selecionada, então, seus seletores passam para o estado "indisponível" automaticamente.

![Seleção includente](http://www.richards.com.br/filtro/rch-filtro-12.png?abc)

A passagem do estado "disponível" para o estado "indisponível" deve ocorrer numa animação suave. Isso vai deixar clara a relação de ação e reação entre a seleção da cor e a consequente indisponibilidade de tamanhos.

A ativação ou desativação dos seletores do tipo checkbox devem atualizar automaticamente as características de mínimo e máximo no seletor de faixa de preço, conforme descrito a seguir.

### Lógica do temporizador

A ativação dos seletores do tipo checkbox não são aplicadas imediatamente após o clique. Quando um seletor é ativado, tem-se início um temporizador que aguarda 2s antes de aplicar o filtro efetivamente e o sistema buscar novos resultados. Isso serve para que o cliente tenha algum tempo para adicionar novos filtros.

![Temporização dos seletores](http://www.richards.com.br/filtro/rch-filtro-13.png?abc)

Durante a temporização e também durante o período que o sistema demora para carregar os resultados, a área de produtos na arara é esmaecida e um ícone de "loading" aparece fixo ao centro, independente da rolagem da página.

![Temporização dos seletores](http://www.richards.com.br/filtro/rch-filtro-17.png?abc)

Durante a temporização, se houver alguma nova interação com seletores checkbox, ou se alguma caixa de critério for aberta, a temporização é adiada e a contagem dos 2s é reiniciada.

![Temporização adiada](http://www.richards.com.br/filtro/rch-filtro-14.png?abc)

Durante a temporização, se houver alguma interação com um dos botões da área de resumo, com o botão "limpar", ou com o botão "filtrar", a temporização é interrompida e o filtro é aplicado imediatamente.

![Temporização interrompida](http://www.richards.com.br/filtro/rch-filtro-15.png?abc)

Durante a temporização, se a configuração dos seletores gerar a mesma url em que o cliente já está, a temporização é abortada e nenhum filtro é aplicado. Um exemplo seria se o cliente ativasse um seletor, desistisse e o desativasse durante a temporização.

![Temporização interrompida](http://www.richards.com.br/filtro/rch-filtro-16.png?abc)

### Comportamento do seletor de faixa de preço

O seletor de faixa de preço tem um comportamento diferente dos demais. Ele contém uma linha horizontal que representa a variação possível de preços da seção, do produto mais barato até o mais caro, e dois controles arrastáveis ao longo da linha, que delimitam a faixa de preço escolhida pelo usuário.

![Seletor de faixa de preço](http://www.richards.com.br/filtro/rch-filtro-04.png?abcd)

O controle que representa o preço mínimo move-se a partir da esquerda da faixa, e o controle que representa o máximo move-se a partir da direita. Como o preço máximo nunca pode ser menor do que o mínimo, é impossível, na interface, que os controles se atravessem, o limite de um é o posicionamento do outro.

Ao contrário dos seletores checkbox, que funcionam com um temporizador, o seletor de faixa de preço só é ativado quando o cliente clica no botão "filtrar". Quando isso ocorre, o filtro é aplicado imediatamente e os resultados são carregados.

A cada novo carregamento, os valores mínimo e máximo são recalculados. O valor mínimo torna-se ou o valor selecionado pelo cliente (se tiver ocorrido uma seleção), ou o valor do produto mais barato na página. A mesma lógica vale para o valor máximo. Um comportamento semelhante pode ser verificado [no site da Dafiti](http://www.dafiti.com.br/roupas-masculinas/camisetas/).

![Seletor de faixa de preço](http://www.richards.com.br/filtro/rch-filtro-18.png?abcd)

O clique feito diretamente na linha horizontal aproxima o controle que estiver mais próximo para o ponto do clique.

![Seletor de faixa de preço](http://www.richards.com.br/filtro/rch-filtro-19.png?abcd)

As caixas de texto que mostram os valores mínimo e máximo disponíveis são editáveis pelo teclado. No entanto, a entrada de valores menores do que o mínimo disponível ou maiores do que o máximo disponível devem ser corrigidas, sendo substituídas pelos valores mínimo e máximo, respectivamente.

### Comportamento específico do seletor de tamanhos

O seletor de tamanhos tem a peculiaridade de apresentar opções de seleção distintas que podem ter os mesmos nomes. Por exemplo, o número "40" pode referir-se tanto ao número de um calçado adulto quanto ao tamanho de uma calça; já o número "2" que pode indicar tanto o tamanho de uma camisa adulta quanto o tamanho de um produto infantil indicado para crianças de dois anos.

Quando o cliente encontra-se numa seção do site que permite filtrar muitos produtos ao mesmo tempo, surge a necessidade de desfazer essa ambiguidade. Nesses casos, deve-se agrupar os tamanhos em categoriais, de modo que os valores repetidos estejam em grupos distintos.

![Seletor de tamanho](http://www.richards.com.br/filtro/rch-filtro-20.png)

No exemplo anterior, a divisão de tamanhos em quatro categorias macro (roupas, roupas infantis, calçados e acessórios e calçados e acessórios infantis) faz esse papel.

A divisão dos tamanhos em categorias só deve ocorrer na tela de acordo com a necessidade. Se o cliente encontra-se na seção Masculino Adulto, não existe a necessidade de mostrar seletores relacionados aos produtos infantis.

![Seletor de tamanho](http://www.richards.com.br/filtro/rch-filtro-21.png)

O sistema deve compreender que a seleção de um valor não implica na seleção automática de seu par repetido em outro grupo.

![Seletor de tamanho](http://www.richards.com.br/filtro/rch-filtro-22.png)

O texto da área de resumo também deve ser alterado se houver ambiguidade de tamanhos. No exemplo a seguir, especifica-se o tipo de cada um dos tamanhos "40".

![Área de resumo](http://www.richards.com.br/filtro/rch-filtro-26.png)

Interação entre as partes
-------------------------

Todas as partes do filtro se comunicam entre si. Os valores atualizados na seleção de faixa de preço podem indisponibilizar seletores de cor, assim como a ativação de um seletor de cor pode restringir a faixa de preço. A remoção de um filtro realizado na área de resumo é replicada na na caiza de categoria, e vice-versa. às vezes, os botões são atalhos para outros: o botão "limpar" equivale a desativar todos os seletores dentro de uma caixa ao mesmo tempo.

Versão de celular
-----------------

A interação com o filtro no celular é composta por três etapas:

![Etapas da versão de celular](http://www.richards.com.br/filtro/rch-filtro-28.png?abc)

* Acesso ao filtro na tela de listagem de produtos;
* Escolha de um dos filtros;
* Alterações no filtro escolhido.

### Tela de listagem de produtos

Na tela de listagem de produtos, a única interação possível com o filtro é o seu acesso pelo botão "Filtros". Um indicador numérico aparece do lado direito do botão caso haja um ou mais filtros ativos no momento, sendo que o filtro por faixa de preço conta como um filtro ativo. O clique no botão "Filtros" deve abrir a tela de escolha com uma animação de baixo para cima.

![Listagem de produtos](http://www.richards.com.br/filtro/rch-filtro-31.png?abc)

### Tela de escolha de filtros

Na tela de escolha de filtros, é possível interagir usando o botão "Limpar", o botão "Voltar" e os botões que levam às telas dos filtros em si.

* O botão "Limpar" só é habilitado quando há um ou mais filtros ativos, do contrário, ele fica desabilitado, visualmente esmaecido. O clique no botão "Limpar", nessa tela, limpa **todos** os filtros já aplicados pelo cliente, mas não leva à tela anterior.
* Apenas o clique no botão "Voltar" leva à tela anterior, numa animação inversa à de abertura, ou seja, de cima para baixo.
* O clique em qualquer um dos botões de filtro leva à sua tela correspondente.

![Escolha de filtros](http://www.richards.com.br/filtro/rch-filtro-32.png?abc)

O numeral que indica a quantidade de filtros ativos também está presente nessa tela, caso haja algum.

Resumos indicando os filtros ativos aparecem (truncados, se for necessário) ao lado de cada critério. Por exemplo, na imagem anterior, ao lado de "Cor" temos, "Azul, verde ou vermelho".

As regras para a construção da frase de resumo são as mesmas utilizadas no desktop, inclusive no caso de haver tamanhos ambíguos. Ainda na imagem anterior, ao lado de "Tamanho" temos "5, 40 (roupas) ou 40 (calçados e acessórios)". Frases grandes como essa provavelmente terão que ser truncadas com reticências, o que vai depender do lay-out.

### Tela do filtro em si

A tela do filtro em si também tem o botão "Limpar", mas nessa tela, seu clique limpa apenas os filtros relacionados ao critério que está sendo visualizado. Por exemplo, se o usuário está em "Tamanho" e clica no botão "Limpar", apenas os filtros de tamanho serão removidos.

![Botão "Limpar" na tela do filtro](http://www.richards.com.br/filtro/rch-filtro-29.png?abc)

Na tela do filtro, o botão "Voltar" é substituído pelo botão "Aplicar", cujo clique aplica a seleção de filtros e volta para a tela anterior. É importante notar que não há temporizador na interação do filtro pelo celular, todas as ações são confirmadas pelo botão "Aplicar".

![Botão "Aplicar" na tela do filtro](http://www.richards.com.br/filtro/rch-filtro-30.png?abc)

A contagem de filtros ativos, ao lado do título "Filtros" deve ser alterada simultaneamente com a seleção feita pelo usuário, em tempo real.

Os seletores no celular se comportam da mesma maneira que no desktop.

![Funcionamento como no desktop](http://www.richards.com.br/filtro/rch-filtro-25.png?ab)
